﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Threading;
using Chat_Service;
using System.ServiceModel.Dispatcher;


namespace Chat_Host {
    public class Program {
        public static List<Message> unreadMessages = new List<Message>();
        public static Thread MonitorThread;
        static void Main(string[] args) {
            using (ServiceHost host = new ServiceHost(typeof(Chat_Service.Service))) {
                host.Open();

                MonitorThread = new Thread(MonitorAction);
                MonitorThread.Name = "MonitorThread";
                MonitorThread.IsBackground = true;
                MonitorThread.Start();

                //MonitorThread = new Thread(UnreadMessagesAction);
                //MonitorThread.Name = "UnreadMessagesThread";
                //MonitorThread.IsBackground = true;
                //MonitorThread.Start();

                Console.CursorVisible = false;
                Console.ReadKey();

                host.Close();
            }
        }

        //static void UnreadMessagesAction() {
        //    while (true) {
        //        Chat_DBEntities context = new Chat_DBEntities();
        //        unreadMessages = context.Messages.Where(m => m.Status.Equals("unread")).ToList();
        //    }
        //}

        static void MonitorAction() {
            while (true) {
                Thread.Sleep(500);
                Console.Clear();
                Console.WriteLine("СЕРВЕР ЗАПУЩЕН");
                Console.WriteLine("==============");
                Console.WriteLine();


                Chat_DBEntities context = new Chat_DBEntities();
                foreach (User _user in context.Users) {
                    Console.WriteLine("{0,-12} - {1}",_user.UserName, _user.Status);
                }
                Console.WriteLine();

                unreadMessages = context.Messages.Where(m => m.Status.Equals("unread")).ToList();
                foreach (Message _message in unreadMessages) {
                    Console.WriteLine(_message.Text);
                }
            }
        }
    }
}