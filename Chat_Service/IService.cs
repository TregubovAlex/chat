﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Chat_Service {
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени интерфейса "IService" в коде и файле конфигурации.
    [ServiceContract(CallbackContract = typeof(ICallBack))]
    public interface IService {
        [OperationContract(IsOneWay=true)]
        void SendMessage(Message _message);

        [OperationContract(IsOneWay = true)]
        void MakeOnline(User _user);

        [OperationContract(IsOneWay = true)]
        void MakeOffline(User _user);
    }

    //[DataContract]
    //public class EventDataType {
    //    [DataMember]
    //    public string ClientName { get; set; }

    //    [DataMember]
    //    public string EventMessage { get; set; }
    //}

    public interface ICallBack {
        [OperationContract]
        void GetUserID(Guid _guid);

        [OperationContract]
        void TakeMessage(Message _message);

        [OperationContract]
        void UpdateOnlineUsersList(List<User> _onlineUsersList);
    }
}