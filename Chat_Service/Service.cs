﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Chat_Service {
    // ПРИМЕЧАНИЕ. Команду "Переименовать" в меню "Рефакторинг" можно использовать для одновременного изменения имени класса "Service" в коде и файле конфигурации.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class Service : IService {
        private static List<User> onlineUsersList = new List<User>();
        private static Dictionary<Guid, ICallBack> clientsDictionary = new Dictionary<Guid, ICallBack>();
        private static object locker = new object();

        public void SendMessage(Message _message) {
            lock (locker) {
                Chat_DBEntities context = new Chat_DBEntities();
                context.Messages.Add(_message);
                context.SaveChanges();

                List<Guid> inactiveClients = new List<Guid>();
                foreach (var client in clientsDictionary) {
                    //if (client.Key != _message.SenderID) {
                    try {
                        client.Value.TakeMessage(_message);
                    }
                    catch (Exception) {
                        inactiveClients.Add(client.Key);
                    }
                    //}
                }

                if (inactiveClients.Count > 0) {
                    foreach (Guid clientID in inactiveClients) {
                        clientsDictionary.Remove(clientID);
                    }
                }
            }
        }

        public void MakeOnline(User _user) {
            Chat_DBEntities context = new Chat_DBEntities();
            if (context.Users.Where(u => u.UserDomainName.Equals(_user.UserDomainName) && u.UserName.Equals(_user.UserName)).Count() == 1) {
                context.Users.Where(u => u.UserDomainName.Equals(_user.UserDomainName) && u.UserName.Equals(_user.UserName)).First().Status = "online";
                _user.ID = context.Users.Where(u => u.UserDomainName.Equals(_user.UserDomainName) && u.UserName.Equals(_user.UserName)).First().ID;
            }
            else {
                context.Users.Add(_user);
            }
            context.SaveChanges();

            ICallBack callback = OperationContext.Current.GetCallbackChannel<ICallBack>();
            lock (locker) {
                //remove the old client
                if (clientsDictionary.Keys.Contains(_user.ID))
                    clientsDictionary.Remove(_user.ID);
                clientsDictionary.Add(_user.ID, callback);
            }
            OperationContext.Current.GetCallbackChannel<ICallBack>().GetUserID(_user.ID);

            onlineUsersList.Add(_user);
            List<Guid> inactiveClients = new List<Guid>();
            foreach (var client in clientsDictionary) {
                //if (client.Key != _user.ID) {
                try {
                    client.Value.UpdateOnlineUsersList(new List<User>(onlineUsersList));
                }
                catch (Exception) {
                    inactiveClients.Add(client.Key);
                }
                //}

                if (inactiveClients.Count > 0) {
                    foreach (Guid clientID in inactiveClients) {
                        clientsDictionary.Remove(clientID);
                    }
                }
            }
        }

        public void MakeOffline(User _user) {
            Chat_DBEntities context = new Chat_DBEntities();
            context.Users.Where(u => u.ID.Equals(_user.ID)).First().Status = "offline";
            context.SaveChanges();

            ICallBack callback = OperationContext.Current.GetCallbackChannel<ICallBack>();
            lock (locker) {
                if (clientsDictionary.Keys.Contains(_user.ID))
                    clientsDictionary.Remove(_user.ID);
            }

            List<User> temp = new List<User>(onlineUsersList);
            foreach (User u in temp) {
                if (u.ID.Equals(_user.ID)) {
                    onlineUsersList.Remove(u);
                }
            }

            List<Guid> inactiveClients = new List<Guid>();
            foreach (var client in clientsDictionary) {
                if (client.Key != _user.ID) {
                    try {
                        client.Value.UpdateOnlineUsersList(new List<User>(onlineUsersList));
                    }
                    catch (Exception) {
                        inactiveClients.Add(client.Key);
                    }
                }

                if (inactiveClients.Count > 0) {
                    foreach (Guid clientID in inactiveClients) {
                        clientsDictionary.Remove(clientID);
                    }
                }
            }
        }
    }
}