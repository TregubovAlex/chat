﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Chat_Service;
using System.ServiceModel;
using System.Collections.ObjectModel;

namespace Chat_Client {
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, Chat_Service_Reference.IServiceCallback {
        Chat_Service_Reference.ServiceClient client;
        User user;
        //public List<User> onlineUsersList = new List<User>();
        public ObservableCollection<User> onlineUsers;

        public MainWindow() {
            InitializeComponent();
            MainWindow.GetWindow(this).Title = System.Environment.UserDomainName + "\\" + System.Environment.UserName;

            InstanceContext ic = new InstanceContext(this);
            client = new Chat_Service_Reference.ServiceClient(ic, "NetTcpBinding_IService");
            user = new User {
                ID = Guid.NewGuid(),
                Status = "online",
                UserDomainName = System.Environment.UserDomainName,
                UserName = System.Environment.UserName
            };
            //_user.ID = 
            client.MakeOnline(user);
        }

        public void GetUserID(Guid _guid) {
            user.ID = _guid;
        }

        private void SendMessage_Button_Click(object sender, RoutedEventArgs e) {
            Message _message = new Message {
                ID = Guid.NewGuid(),
                CreatedDateTime = DateTime.Now,
                Status = "unread",
                SenderID = user.ID,
                //RecipientID = "",
                Text = Input_TextBox.Text
            };
            client.SendMessage(_message);
        }
        public void TakeMessage(Message _message) {
            History_TextBlock.Text += _message.Text;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            client.MakeOffline(user);
            client.Close();
        }

        public void UpdateOnlineUsersList(User[] _onlineUsersList) {
            onlineUsers = new ObservableCollection<User>(_onlineUsersList);
            this.Users_DataGrid.ItemsSource = onlineUsers;
        }
    }
}